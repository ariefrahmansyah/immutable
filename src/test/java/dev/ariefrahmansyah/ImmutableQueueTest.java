package dev.ariefrahmansyah;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ImmutableQueueTest {

    @Test
    void testQueue() throws Exception {
        // Q: []
        IImmutableQueue<Integer> queue = ImmutableQueue.getEmptyQueue();
        assertEquals(true, queue.isEmpty());

        // Q: [1]
        queue = queue.enQueue(1);
        assertEquals(false, queue.isEmpty());

        // Q: [1, 2]
        queue = queue.enQueue(2);
        Integer currentHead = queue.head();
        assertEquals(1, currentHead);

        // Q: [2]
        queue = queue.deQueue();
        currentHead = queue.head();
        assertEquals(2, currentHead);

        // Q: []
        queue = queue.deQueue();
        assertEquals(true, queue.isEmpty());
    }
}