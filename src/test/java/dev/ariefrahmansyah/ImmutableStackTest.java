package dev.ariefrahmansyah;

import static org.junit.jupiter.api.Assertions.*;

class ImmutableStackTest {

    @org.junit.jupiter.api.Test
    void testStack() throws Exception {
        // Stack: []
        IImmutableStack<Integer> stack = ImmutableStack.getEmptyStack();
        assertEquals(true, stack.isEmpty());

        // Stack: [1]
        stack = stack.push(1);
        assertEquals(false, stack.isEmpty());

        // Stack: [1, 2]
        stack = stack.push(2);
        Integer currentHead = stack.head();
        assertEquals(2, currentHead);

        // Stack: [1]
        stack = stack.pop();
        assertEquals(false, stack.isEmpty());

        // Stack: []
        stack = stack.pop();
        assertEquals(true, stack.isEmpty());

        // Stack: [5]
        stack = stack.push(5);

        // Stack: [5, 4]
        stack = stack.push(4);

        // Stack: [5, 4, 3]
        stack = stack.push(3);

        // Stack: [3, 4, 5]
        stack = stack.reverse();

        currentHead = stack.head();
        assertEquals(5, currentHead);
        assertEquals(5, currentHead);
    }
}