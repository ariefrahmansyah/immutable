package dev.ariefrahmansyah;

public interface IImmutableStack<T> {
    /**
     * Gets a value indicating whether this is the empty stack.
     *
     * @return true if this stack is empty; otherwise, false.
     */
    boolean isEmpty();

    /**
     * Pushes an element onto a stack and returns the new stack.
     *
     * @param t the element to push onto stack.
     * @return the new stack.
     */
    IImmutableStack<T> push(T t);

    /**
     * Pops the top element off the stack.
     *
     * @return the new stack; never null.
     * @throws Exception when the stack is empty.
     */
    IImmutableStack<T> pop() throws Exception;

    /**
     * Gets the element on the top of the stack.
     *
     * @return the element on the top of the stack.
     * @throws Exception when the stack is empty.
     */
    T head() throws Exception;

    /**
     * Reverses the order of a stack.
     *
     * @return the reversed stack.
     * @throws Exception when the stack is empty.
     */
    IImmutableStack<T> reverse() throws Exception;
}
