package dev.ariefrahmansyah;

public interface IImmutableQueue<T> {
    /**
     * Gets a value indicating whether this is the empty queue.
     *
     * @return true if this queue is empty; otherwise, false.
     */
    boolean isEmpty();

    /**
     * Adds an element to the back of the queue.
     *
     * @param t the element.
     * @return the new queue.
     */
    IImmutableQueue<T> enQueue(T t);

    /**
     * Returns a queue that is missing the front element.
     *
     * @return a queue, never null.
     * @throws Exception when the queue is empty.
     */
    IImmutableQueue<T> deQueue() throws Exception;

    /**
     * Gets the element at the front of the queue.
     *
     * @return the element at the front of the queue.
     * @throws Exception when the queue is empty.
     */
    T head() throws Exception;
}
