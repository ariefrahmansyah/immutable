package dev.ariefrahmansyah;

/**
 * An immutable stack.
 *
 * @param <T> the type of element stored by the stack.
 */
public final class ImmutableStack<T> implements IImmutableStack<T> {

    /**
     * The element on the top of the stack.
     */
    private final T head;

    /**
     * A stack that contains the rest of the elements (under the top element).
     */
    private final IImmutableStack<T> tail;

    /**
     * Initializes a new instance of the ImmutableStack<T> class.
     *
     * @param head the head element on the stack.
     * @param tail the rest of the elements on the stack.
     */
    private ImmutableStack(T head, IImmutableStack<T> tail) {
        this.head = head;
        this.tail = tail;
    }

    /**
     * Gets a value indicating whether this is the empty stack.
     *
     * @return true if this stack is empty; otherwise, false.
     */
    public final boolean isEmpty() {
        return this.tail == null;
    }

    /**
     * Pushes an element onto a stack and returns the new stack.
     *
     * @param t the element to push onto stack.
     * @return the new stack.
     */
    public final IImmutableStack<T> push(T t) {
        return new ImmutableStack<T>(t, this);
    }

    /**
     * Pops the top element off the stack.
     *
     * @return the new stack; never null.
     * @throws Exception when the stack is empty.
     */
    public final IImmutableStack<T> pop() throws Exception {
        if (this.isEmpty()) {
            throw new Exception("Stack is empty.");
        }

        return this.tail;
    }

    /**
     * Gets the element on the top of the stack.
     *
     * @return the element on the top of the stack.
     * @throws Exception when the stack is empty.
     */
    public final T head() throws Exception {
        if (this.isEmpty()) {
            throw new Exception("Stack is empty.");
        }

        return this.head;
    }

    /**
     * Reverses the order of a stack.
     *
     * @return the reversed stack.
     * @throws Exception when the stack is empty.
     */
    public final IImmutableStack<T> reverse() throws Exception {
        if (this.isEmpty()) {
            throw new Exception("Stack is empty.");
        }

        IImmutableStack<T> stack = getEmptyStack();

        for (IImmutableStack<T> f = this; !f.isEmpty(); f = f.pop()) {
            stack = stack.push(f.head());
        }

        return stack;
    }

    /**
     * Gets the empty stack, upon which all stacks are built.
     *
     * @return the empty stack.
     */
    public final static IImmutableStack getEmptyStack() {
        return EmptyStack.getInstance();
    }

    /**
     * The singleton empty stack.
     *
     * @param <T> the type of element stored by the stack.
     */
    private static final class EmptyStack<T> implements IImmutableStack<T> {

        private final static EmptyStack emptyStack = new EmptyStack();

        public final static EmptyStack getInstance() {
            return emptyStack;
        }

        public final boolean isEmpty() {
            return true;
        }

        public final IImmutableStack<T> push(T t) {
            return new ImmutableStack<T>(t, this);
        }

        public final IImmutableStack<T> pop() throws Exception {
            throw new Exception("Stack is empty.");
        }

        public final T head() throws Exception {
            throw new Exception("Stack is empty.");
        }

        public final IImmutableStack<T> reverse() throws Exception {
            throw new Exception("Stack is empty.");
        }
    }
}
