package dev.ariefrahmansyah;

/**
 * An immutable queue.
 *
 * @param <T> the type of element stored by the queue.
 */
public final class ImmutableQueue<T> implements IImmutableQueue {

    /**
     * The end of the queue that enqueued elements are pushed onto.
     */
    private final IImmutableStack<T> backwards;

    /**
     * The end of the queue from which elements are dequeued.
     */
    private final IImmutableStack<T> forwards;

    /**
     * Initializes a new instance of the ImmutableQueue<T> class.
     *
     * @param forwards  the forwards stack.
     * @param backwards the backwards stack.
     */
    private ImmutableQueue(IImmutableStack<T> forwards, IImmutableStack<T> backwards) {
        this.forwards = forwards;
        this.backwards = backwards;
    }

    /**
     * Gets a value indicating whether this queue instance is empty.
     *
     * @return true if this queue instance is empty; otherwise, false.
     */
    public final boolean isEmpty() {
        return this.forwards.isEmpty();
    }

    /**
     * Adds an element to the back of the queue.
     *
     * @param t the element.
     * @return the new queue
     */
    public final IImmutableQueue<T> enQueue(Object t) {
        return new ImmutableQueue<T>(this.forwards, this.backwards.push((T) t));
    }

    /**
     * Returns a queue that is missing the front element.
     *
     * @return a queue, never null.
     * @throws Exception when the queue is empty.
     */
    public final IImmutableQueue<T> deQueue() throws Exception {
        if (this.isEmpty()) {
            throw new Exception("Queue is empty.");
        }

        IImmutableStack<T> f = this.forwards.pop();
        if (!f.isEmpty()) {
            return new ImmutableQueue<T>(f, this.backwards);
        } else if (this.backwards.isEmpty()) {
            return ImmutableQueue.getEmptyQueue();
        } else {
            return new ImmutableQueue<T>(this.backwards.reverse(), ImmutableStack.getEmptyStack());
        }
    }

    /**
     * Gets the element at the front of the queue.
     *
     * @return the element at the front of the queue.
     * @throws Exception when the queue is empty.
     */
    public final T head() throws Exception {
        return this.forwards.head();
    }

    /**
     * Gets the empty queue, upon which all queue are built.
     *
     * @return the empty queue.
     */
    public final static IImmutableQueue getEmptyQueue() {
        return EmptyQueue.getInstance();
    }

    /**
     * The singleton empty queue.
     *
     * @param <T> the type of element stored by the queue.
     */
    private static final class EmptyQueue<T> implements IImmutableQueue<T> {

        @SuppressWarnings("rawtypes")
        private final static EmptyQueue emptyQueue = new EmptyQueue();

        @SuppressWarnings("rawtypes")
        public final static EmptyQueue getInstance() {
            return emptyQueue;
        }

        @SuppressWarnings("unchecked")
        public final IImmutableQueue<T> enQueue(T t) {
            return new ImmutableQueue<T>(ImmutableStack.getEmptyStack().push(t), ImmutableStack.getEmptyStack());
        }

        public final IImmutableQueue<T> deQueue() throws Exception {
            throw new Exception("Queue is empty.");
        }

        public final T head() throws Exception {
            throw new Exception("Queue is empty.");
        }

        public final boolean isEmpty() {
            return true;
        }
    }
}
